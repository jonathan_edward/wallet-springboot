package com.example.wallet.customer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class CustomerController {
  private CustomerService customerService;

  @Autowired
  public CustomerController(CustomerService customerService) {
    this.customerService = customerService;
  }

  @PostMapping(path = "/customers")
  public @ResponseBody String createCustomer(@RequestBody Customer customer) {
    customerService.saveCustomer(customer);
    return "Created";
  }

}
