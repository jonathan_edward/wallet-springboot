package com.example.wallet.customer;

import lombok.*;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;

@Data
@Entity
@NoArgsConstructor
@RequiredArgsConstructor
public class Customer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private BigInteger id;

  @NonNull private String name;
  @NonNull private Date birthdate;
  @NonNull private String address;
}
