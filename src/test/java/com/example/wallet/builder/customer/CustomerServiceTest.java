package com.example.wallet.builder.customer;

import com.example.wallet.builder.CustomerBuilder;
import com.example.wallet.customer.Customer;
import com.example.wallet.customer.CustomerService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@DisplayName("Customer Service Test")
public class CustomerServiceTest {
  @Autowired
  private CustomerService customerService;

  @Nested
  @DisplayName("Save Customer")
  class SaveCustomer {
    @Test
    @DisplayName("Expected to create new customer")
    void saveCustomer_expectToCreateNewCustomer() {
      Customer expectedCustomer = CustomerBuilder.build();

      Customer actualCustomer = customerService.saveCustomer(expectedCustomer);

      assertEquals(expectedCustomer, actualCustomer);
    }
  }
}
