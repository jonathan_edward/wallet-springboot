package com.example.wallet.builder.customer;

import com.example.wallet.builder.CustomerBuilder;
import com.example.wallet.customer.Customer;
import com.example.wallet.customer.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@DisplayName("Customer Controller Test")
class CustomerControllerTest {
  @Autowired private MockMvc mockMvc;
  @MockBean private CustomerService customerService;

  @Nested
  @DisplayName("Create Customer")
  class CreateCustomer {
    @Test
    @DisplayName("Expect to successful ")
    void createCustomer_expectToSuccessful() throws Exception {
      ObjectMapper objectMapper = new ObjectMapper();
      Customer customer = CustomerBuilder.build();

      mockMvc
          .perform(
              post("/customers")
                  .contentType(MediaType.APPLICATION_JSON)
                  .content(objectMapper.writeValueAsString(customer)))
          .andExpect(status().isOk());

      verify(customerService, times(1)).saveCustomer(customer);
    }
  }
}
