package com.example.wallet.builder;

import com.example.wallet.customer.Customer;
import com.github.javafaker.Faker;

import java.util.Date;

public class CustomerBuilder {

  private static Faker faker = new Faker();

  public static Customer build() {
    String name = faker.name().fullName();
    Date birthdate = faker.date().birthday();
    String address = faker.address().fullAddress();

    return new Customer(name, birthdate, address);
  }
}
